FROM tomcat
MAINTAINER chetan.nighojkar@veritas.com
COPY target/*.war /usr/local/tomcat/webapps/CounterWebApp.war
CMD ["catalina.sh", "run"]
